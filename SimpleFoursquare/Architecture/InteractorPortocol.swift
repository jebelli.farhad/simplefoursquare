//
//  InteracrotPortocol.swift
//  Sportiv
//
//  Created by farhad jebelli on 9/19/19.
//  Copyright © 2019 Sportiv. All rights reserved.
//

import UIKit

protocol InteractorProtocol: class {
    
    func viewControllerCreated()
}

extension InteractorProtocol {
    func viewControllerCreated() {}
}

extension InteractorProtocol {
    func build<ViewController: ViewControllerProtocol>(viewControllerType type: ViewController.Type = ViewController.self) -> ViewController where ViewController.Intractor == Self {
        let viewController: ViewController = UIStoryboard.instantiateViewController(name: ViewController.storyboardName)
        viewController.intractor = self
        viewControllerCreated()
        return viewController
    }
}
