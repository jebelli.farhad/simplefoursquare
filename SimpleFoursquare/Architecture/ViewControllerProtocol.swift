//
//  ViewControllerProtocol.swift
//  Sportiv
//
//  Created by farhad jebelli on 9/19/19.
//  Copyright © 2019 Sportiv. All rights reserved.
//

import UIKit

protocol ViewControllerProtocol: UIViewController {
    associatedtype Intractor: InteractorProtocol
    var intractor: Intractor! {get set}
    static var storyboardName: String {get}
}

extension ViewControllerProtocol {
    static var storyboardName: String {
        String(describing: Self.self)
    }
}
