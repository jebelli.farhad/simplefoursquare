//
//  LocationManager.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/19/20.
//

import Foundation
import CoreLocation
import Combine
protocol LocationManagerProtocol {
    var lastCoordinate: CLLocationCoordinate2D?{get}
    var lastLocationPublisher: AnyPublisher<CLLocationCoordinate2D, Never> {get}
}

class LocationManager: NSObject, LocationManagerProtocol {
    static let shared = LocationManager()
    private var manager: CLLocationManager
    
    @Published var lastLocation: CLLocation?
    var lastCoordinate: CLLocationCoordinate2D? {
        lastLocation?.coordinate
    }
    var lastLocationPublisher: AnyPublisher<CLLocationCoordinate2D, Never> {
        $lastLocation.compactMap({$0?.coordinate}).eraseToAnyPublisher()
    }
    
    override init() {
        manager = CLLocationManager()
        super.init()
        manager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        manager.requestWhenInUseAuthorization()
        manager.delegate = self
        manager.startUpdatingLocation()
    }
}

extension LocationManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let last = lastLocation {
            if let new = locations.last, new.distance(from: last) > 100 {
                lastLocation = new
            }
        } else {
            lastLocation = locations.last
        }
        
    }
    

    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        
    }
}
