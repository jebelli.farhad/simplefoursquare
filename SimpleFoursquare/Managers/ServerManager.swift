//
//  ServerManager.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation
import Combine

protocol URLConvertibale {
    func asURL() throws -> URL
}

protocol ServerManagerProtocol {
    func fetchData<T: Decodable>(url convertible: URLConvertibale) -> AnyPublisher<T, Error>
}

struct ServerManager: ServerManagerProtocol {
    func fetchData<T: Decodable>(url convertible: URLConvertibale) -> AnyPublisher<T, Error> {
        let session = URLSession(configuration: .ephemeral)
        return Just(convertible).tryMap({try $0.asURL()}).flatMap({session.dataTaskPublisher(for: $0).mapError({$0 as Error})}).tryMap { response in
                guard let httpURLResponse = response.response as? HTTPURLResponse,
                      httpURLResponse.statusCode == 200
                else {
                    throw try JSONDecoder().decode(FoursquareError.self, from: response.data)
                }
                
                return response.data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
