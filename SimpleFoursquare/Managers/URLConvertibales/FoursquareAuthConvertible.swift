//
//  FoursquareAuthConvertible.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation

struct FoursquareAuthConvertible: URLConvertibale {
    let url: URLConvertibale
    init(url: URLConvertibale) {
        self.url = url
    }
    func asURL() throws -> URL {
        var components = try URLComponents.create(from: try url.asURL(), resolvingAgainstBaseURL: true)
        var query = components.queryItems ?? []
        if CLIENT_ID.isEmpty || CLIENT_SECRET.isEmpty {
            fatalError("fill CLIENT_ID and CLIENT_SECRET in Config.swift")
        }
        query.append(URLQueryItem(name: "client_id", value: CLIENT_ID))
        query.append(URLQueryItem(name: "client_secret", value: CLIENT_SECRET))
        components.queryItems = query
        return try components.asURL()
    }
}
