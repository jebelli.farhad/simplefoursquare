//
//  VenueRecommendationsURL.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation
import CoreLocation
private var path = "v2/venues/explore"
struct VenueRecommendationsURL: URLConvertibale {
    let coordinate: CLLocationCoordinate2D
    let offset: Int
    let limit: Int
    init(coordinate: CLLocationCoordinate2D, limit: Int, offset: Int) {
        self.coordinate = coordinate
        self.offset = offset
        self.limit = limit
    }
    func asURL() throws -> URL {
        let url = URL(string: path, relativeTo: BASE_URL)!
        var components = try URLComponents.create(from: url, resolvingAgainstBaseURL: true)
        var query = [URLQueryItem]()
        query.append(.init(name: "ll", value: "\(coordinate.latitude),\(coordinate.longitude)"))
        query.append(.init(name: "v", value: VERSION_DATE))
        query.append(.init(name: "v", value: VERSION_DATE))
        query.append(.init(name: "limit", value: "\(limit)"))
        query.append(.init(name: "offset", value: "\(offset)"))
        
        components.queryItems = query
        return try components.asURL()
    }
}
