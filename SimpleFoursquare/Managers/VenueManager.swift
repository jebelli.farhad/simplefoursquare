//
//  VenueManager.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation
import Combine
import CoreLocation
import Geohash
import CoreData
protocol VenueManagerProtocol {
    func fetchVenueList(coordinate: CLLocationCoordinate2D, page: Int) -> AnyPublisher<FoursquareLocalModel, Error>
}

struct VenueManager: VenueManagerProtocol {
    private let limit = 20
    private var databaseManager: VenueDataBase
    
    init() {
        self.databaseManager = VenueDataBase()
    }
    
    func fetchVenueList(coordinate: CLLocationCoordinate2D, page: Int) -> AnyPublisher<FoursquareLocalModel, Error> {
        if let dbModel = databaseManager.fetchVenueList(coordinate: coordinate, page: page), !dbModel.isEmpty {
            return Just(FoursquareLocalModel(data: dbModel)).mapError({$0 as Error}).eraseToAnyPublisher()
        }
        let auth = FoursquareAuthConvertible(url: VenueRecommendationsURL(coordinate: coordinate, limit: limit, offset: limit * page))
        let publisher: AnyPublisher<Foursquare, Error> = ServerManager().fetchData(url: auth)
        let subject = PassthroughSubject<FoursquareLocalModel, Error>()
        
        var resCancellable: AnyCancellable? = nil
        resCancellable = publisher.map(FoursquareLocalModel.init).receive(on: DispatchQueue.main).eraseToAnyPublisher().sink { _ in
            
        } receiveValue: { model in
            databaseManager.removeVenueList(coordinate: coordinate, page: page)
            databaseManager.saveVenueList(coordinate: coordinate, page: page, venues: model.data)
            subject.send(model)
            resCancellable?.cancel()
        }
        return subject.eraseToAnyPublisher()
    }
}

private class VenueDataBase {
    func fetchVenueList(coordinate: CLLocationCoordinate2D, page: Int) -> [VenueLocalModel]? {
        let hash = coordinate.geohash(length: 7)
        return try? fetch(geohash: hash, page: page)
    }
    
    func removeVenueList(coordinate: CLLocationCoordinate2D, page: Int) {
        let hash = coordinate.geohash(length: 7)
        try? remove(geohash: hash, page: page)
    }
    
    func saveVenueList(coordinate: CLLocationCoordinate2D, page: Int, venues: [VenueLocalModel]) {
        let hash = coordinate.geohash(length: 7)
        try? save(venues: venues, page: page, geohash: hash)
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer? = {
        let container = NSPersistentContainer(name: "SimpleFoursquare")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {

            }
        })
        return container
    }()

    private func save(venues: [VenueLocalModel], page: Int, geohash: String) throws {
        guard let container = persistentContainer else {
            throw "Database container not exist"}
        let context = container.viewContext
        context.perform {
            for item in venues {
                let entity = NSEntityDescription.entity(forEntityName: VenueLocalModel.DatabaseTypes.entityName.rawValue, in: context)!
                let object = NSManagedObject(entity: entity, insertInto: context)
                object.setValue(item.name, forKey: VenueLocalModel.DatabaseTypes.name.rawValue)
                object.setValue(item.icon, forKey: VenueLocalModel.DatabaseTypes.icon.rawValue)
                object.setValue(item.lat, forKey: VenueLocalModel.DatabaseTypes.lat.rawValue)
                object.setValue(item.lng, forKey: VenueLocalModel.DatabaseTypes.lng.rawValue)
                object.setValue(geohash, forKey: VenueLocalModel.DatabaseTypes.geohash.rawValue)
                object.setValue(page, forKey: VenueLocalModel.DatabaseTypes.page.rawValue)
            }
            do {
                try context.save()
            } catch {
                print(error)
            }
        }
        
    }

    private func fetch(geohash: String, page: Int) throws -> [VenueLocalModel]? {
        guard let context = persistentContainer?.viewContext else {
            throw "Database container not exist"}
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: VenueLocalModel.DatabaseTypes.entityName.rawValue)
        fetchRequest.predicate = NSPredicate(format: "\(VenueLocalModel.DatabaseTypes.geohash.rawValue) = %@ && \(VenueLocalModel.DatabaseTypes.page.rawValue) = %d", geohash, page)
        let resaults = try context.fetch(fetchRequest)
        let res = resaults.map({ item -> VenueLocalModel in
            let name = item.value(forKey: VenueLocalModel.DatabaseTypes.name.rawValue) as! String
            let icon = item.value(forKey: VenueLocalModel.DatabaseTypes.icon.rawValue) as? URL
            let lat = item.value(forKey: VenueLocalModel.DatabaseTypes.lat.rawValue) as! Double
            let lng = item.value(forKey: VenueLocalModel.DatabaseTypes.lng.rawValue) as! Double
            return VenueLocalModel(name: name, icon: icon, lng: lng, lat: lat)
        })
        return res
        
    }

    private func remove(geohash: String, page: Int) throws {
        guard let context = persistentContainer?.viewContext else {
        throw "Database container not exist"}
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: VenueLocalModel.DatabaseTypes.entityName.rawValue)
        fetchRequest.predicate = NSPredicate(format: "\(VenueLocalModel.DatabaseTypes.geohash.rawValue) = %@ and \(VenueLocalModel.DatabaseTypes.page.rawValue) = %d ", geohash, page)
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        try context.execute(deleteRequest)
        try context.save()
    }
}
//
