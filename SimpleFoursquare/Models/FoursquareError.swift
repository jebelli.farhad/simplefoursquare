//
//  FoursquareError.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation

struct FoursquareError: LocalizedError, Decodable {
    let errorDescription: String?
    
    init(error: Error) {
        self.errorDescription = error.localizedDescription
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nested = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .meta)
        errorDescription = try nested.decode(String.self, forKey: .errorDetail)
    }
    enum CodingKeys: String, CodingKey {
        case meta
        case errorDetail
        
    }
}
