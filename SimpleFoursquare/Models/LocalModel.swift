//
//  VenueLocalModel.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/23/20.
//

import Foundation
import CoreLocation
struct FoursquareLocalModel: PaginationData, Codable {
    static var empty = FoursquareLocalModel(data: [])
    var isEmpty: Bool {
        data.isEmpty
    }
    
    var currerntTotal: Int {
        data.count
    }
    
    let data: [VenueLocalModel]
    
    init(foursquare: Foursquare) {
        data = foursquare.data.map(VenueLocalModel.init)
    }
    
    init(data: [VenueLocalModel]) {
        self.data = data
    }
}

struct VenueLocalModel: Codable {
    let name: String
    let icon: URL?
    let lng: Double
    let lat: Double
    
    enum DatabaseTypes: String {
        case name
        case icon
        case lng
        case lat
        case geohash
        case page
        
        case entityName = "VenueItem"
    }
    init (name: String, icon: URL?, lng: Double, lat: Double) {
        self.name = name
        self.icon = icon
        self.lat = lat
        self.lng = lng
    }
    init(venue: Venue) {
        name = venue.name
        icon = try? venue.primaryCategory?.icon.asURL()
        lng = venue.location.coordinate.longitude
        lat = venue.location.coordinate.latitude
    }
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
    
}
