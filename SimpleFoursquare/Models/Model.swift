//
//  Model.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation
import CoreLocation
// MARK: - Pageination
struct Foursquare: PaginationData, Codable {
    var isEmpty: Bool {
        data.count == 0
    }
    
    var totalResults: Int {
        response.totalResults
    }
    
    var currerntTotal: Int {
        data.count
    }
    
    var data: [Venue] {
        response.groups.flatMap({$0.items}).map(\.venue)
    }
    
    let response: Response
}
// MARK: - Response
struct Response: Codable {
    let totalResults: Int
    let groups: [Group]
}

// MARK: - Group
struct Group: Codable {
    let type, name: String
    let items: [GroupItem]
}

// MARK: - GroupItem
struct GroupItem: Codable {
    //let reasons: Reasons
    let venue: Venue
}

// MARK: - Venue
struct Venue: Codable {
    let id, name: String
    let location: Location
    let categories: [Category]
    
    var primaryCategory: Category? {
        categories.filter(\.primary).first
    }
}

// MARK: - Category
struct Category: Codable {
    let id, name, pluralName, shortName: String
    let icon: Icon
    let primary: Bool
}

// MARK: - Icon
struct Icon: Codable, URLConvertibale {
    func asURL() throws -> URL {
        guard let prefix = prefix else {throw "nil icon prefix"}
        guard let suffix = suffix else {throw "nil icon suffix"}
        return URL(string: "\(prefix)32\(suffix)")!
    }
    
    let prefix: String?
    let suffix: String?
}

// MARK: - Location
struct Location: Codable {
    let address: String?
    let lat, lng: Double
    let distance: Int
    let formattedAddress: [String]
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }
}

