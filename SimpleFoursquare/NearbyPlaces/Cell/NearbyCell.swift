//
//  NearbyCell.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/19/20.
//

import UIKit

class NearbyCell: UITableViewCell {
    @IBOutlet var iconView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
}
