//
//  NearbyPlacesIntractor.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import Foundation
import Combine
import CoreLocation
enum NearbyPlacesEvent {
    case next([VenueLocalModel])
    case new
}
class NearbyPlacesInteractor: InteractorProtocol {
    var cancellable = Set<AnyCancellable>()
    let venueManager: VenueManagerProtocol
    let locationManager: LocationManagerProtocol
    var paginationManager: PaginationManager<NearbyProvider>?
    let nearbyPublisher = PassthroughSubject<NearbyPlacesEvent, Never>()
    
    init(venueManager: VenueManagerProtocol = VenueManager(), locationManager: LocationManagerProtocol = LocationManager.shared) {
        self.venueManager = venueManager
        self.locationManager = locationManager
        self.locationManager.lastLocationPublisher.sink {[weak self] location in
            self?.nearbyPublisher.send(.new)
            self?.reset(with: location)
        }.store(in: &cancellable)
    }
    
    func reset(with location: CLLocationCoordinate2D) {
        paginationManager = PaginationManager(provider: NearbyProvider(venueManager: venueManager, coordinate: location))
        paginationManager?.current.sink(receiveValue: {[weak self] event in
            switch event {
            case .next(page: _, model: let model):
                self?.nearbyPublisher.send(.next(model.data))
            case .finished:
                break
            }
        }).store(in: &cancellable)
        paginationManager?.fetchNext()
    }
    
    func fetchNext() {
        if paginationManager == nil, let coordinate = locationManager.lastCoordinate {
            reset(with: coordinate)
        } else if paginationManager != nil {
            paginationManager?.fetchNext()
        }
    }
}
