//
//  NearbyPlacesViewController.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit
import Combine
import SDWebImage
class NearbyPlacesViewController: UITableViewController, ViewControllerProtocol {
    var cancellable = Set<AnyCancellable>()
    var intractor: NearbyPlacesInteractor!
    var unitView: NearbyPlacesView {
        return view as! NearbyPlacesView
    }
    
    var items: [VenueLocalModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Places", comment: "")
        intractor.nearbyPublisher.sink { completion in
            print(completion)
        } receiveValue: {[weak self] event in
            switch event {
            case .next(let items):
                self?.items.append(contentsOf: items)
            case .new:
                self?.items = []
            }
        }.store(in: &cancellable)
        intractor.fetchNext()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NearbyCell
        let item = items[indexPath.row]
        cell.titleLabel?.text = item.name
        cell.iconView?.load(url: item.icon)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let place = items[indexPath.row]
        let detail: PlaceDetailsViewController = PlaceDetailsInteractor(place: place).build()
        show(detail, sender: self)
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == items.count - 1 {
            intractor.fetchNext()
        }
    }
    
}
