//
//  NearbyProvider.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/22/20.
//

import Foundation
import Combine
import CoreLocation
struct NearbyProvider: PaginationDataProvider {
    let coordinate: CLLocationCoordinate2D
    let manager: VenueManagerProtocol
    init(venueManager: VenueManagerProtocol, coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
        self.manager = venueManager
    }
    func fetchData(page: Int) -> AnyPublisher<FoursquareLocalModel, Error> {
        manager.fetchVenueList(coordinate: coordinate, page: page)
    }
    
    
}
