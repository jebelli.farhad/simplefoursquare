//
//  PlaceDetailsIntractor.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/19/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import Foundation

class PlaceDetailsInteractor: InteractorProtocol {
    let place: VenueLocalModel
    init(place: VenueLocalModel) {
        self.place = place
    }

    func viewControllerCreated() {
        
    }
    
}
