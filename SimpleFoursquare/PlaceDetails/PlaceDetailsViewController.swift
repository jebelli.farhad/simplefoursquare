//
//  PlaceDetailsViewController.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/19/20.
//  Copyright (c) 2020 ___ORGANIZATIONNAME___. All rights reserved.
//
//

import UIKit
import MapKit
class PlaceDetailsViewController: UIViewController, ViewControllerProtocol {
    
    var intractor: PlaceDetailsInteractor!
    var unitView: PlaceDetailsView {
        return view as! PlaceDetailsView
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        title = intractor.place.name
        navigationItem.largeTitleDisplayMode = .never
        let anotation = MKPointAnnotation()
        anotation.coordinate = intractor.place.coordinate
        unitView.mapView.addAnnotation(anotation)
        unitView.mapView.setCamera(MKMapCamera(lookingAtCenter: anotation.coordinate, fromDistance: 1000, pitch: 0, heading: 0), animated: false)
    }
}
