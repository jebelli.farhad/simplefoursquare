//
//  String.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation

extension String: LocalizedError {
    public var errorDescription: String? {
        return self
    }
}
