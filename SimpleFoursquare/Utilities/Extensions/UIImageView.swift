//
//  UIImageView.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/19/20.
//

import UIKit
import SDWebImage
extension UIImageView {
    func load(url: URLConvertibale?, completion: ((Bool) -> Void)? = nil)  {
        sd_setImage(with: try? url?.asURL(), placeholderImage: UIImage(systemName: "location.fill")) {[weak self] (image, _, _, _) in
            if let image = image {
                self?.image = image.withRenderingMode(.alwaysTemplate)
                completion?(true)
            } else {
                completion?(false)
            }
        }
    }
}
