//
//  UIStoryboard.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import UIKit

extension UIStoryboard {
    static func instantiateViewController<T: UIViewController>(name: String = String(describing: T.self)) -> T {
        return UIStoryboard(name: name, bundle: Bundle.main).instantiateInitialViewController() as! T
    }
}
