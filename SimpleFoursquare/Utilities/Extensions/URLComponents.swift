//
//  URLComponents.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/18/20.
//

import Foundation

extension URLComponents: URLConvertibale {
    private static var conponentsError = "URLComponents error"
    static func create(from url: URL, resolvingAgainstBaseURL resolve: Bool) throws -> URLComponents {
        guard let conponents = Self(url: url, resolvingAgainstBaseURL: resolve) else {
            throw conponentsError
        }
        return conponents
        
    }
    func asURL() throws -> URL {
        guard let url = self.url else {
            throw Self.conponentsError
        }
        return url
    }
}

extension URL: URLConvertibale {
    func asURL() throws -> URL {
        self
    }
}
