//
//  PaginationManager.swift
//  SimpleFoursquare
//
//  Created by farhad jebelli on 9/22/20.
//

import Foundation
import Combine

protocol PaginationData: Codable {
    associatedtype Data: Codable
    var currerntTotal: Int {get}
    var isEmpty: Bool {get}
    var data: Data { get }
}

enum PaginationEvent<Model: Codable> {
    case next(page: Int, model: Model)
    case finished
}
protocol PaginationDataProvider {
    associatedtype Model: PaginationData
    func fetchData(page: Int) -> AnyPublisher<Model, Error>
}
class PaginationManager<Provider: PaginationDataProvider> {
    var cancellable = Set<AnyCancellable>()
    let provider: Provider
    var page = 0
    var fetchedCount = 0
    var isFinished: Bool = false
    var current = PassthroughSubject<PaginationEvent<Provider.Model>, Never>()
    
    init(provider: Provider) {
        self.provider = provider
    }
    
    func fetchNext() {
        guard !isFinished else {
            return
        }
        let page = self.page
        provider.fetchData(page: page).sink { error in
            
        } receiveValue: {[weak self] model in
            guard let self = self else {return}
            self.current.send(.next(page: page, model: model))
            self.page += 1
            self.fetchedCount += model.currerntTotal
            
            if model.isEmpty {
                self.current.send(.finished)
                self.isFinished = true
            }
        }.store(in: &cancellable)
    }
}
