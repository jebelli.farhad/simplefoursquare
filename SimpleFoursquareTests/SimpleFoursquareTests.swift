//
//  SimpleFoursquareTests.swift
//  SimpleFoursquareTests
//
//  Created by farhad jebelli on 9/18/20.
//

import XCTest
import CoreLocation
import Combine
@testable import SimpleFoursquare

class SimpleFoursquareTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testServerManagerFoursquareError() throws {
        let coordinate = CLLocationCoordinate2D(latitude: 35.773371, longitude: 51.418327)
        let url = VenueRecommendationsURL(coordinate: coordinate)
        let expresion = XCTestExpectation()
        var canselables = Set<AnyCancellable>()
        ServerManager().fetchData(url: url).sink { completion in
            switch completion {
            case .failure:
                expresion.fulfill()
            case .finished:
                break
            }
            expresion.fulfill()
        } receiveValue: { (value: Foursquare) in
            
        }.store(in: &canselables)
        wait(for: [expresion], timeout: 15)
    }
    
    func testServerManager() throws {
        let coordinate = CLLocationCoordinate2D(latitude: 35.773371, longitude: 51.418327)
        let url = VenueRecommendationsURL(coordinate: coordinate)
        let auth = FoursquareAuthConvertible(url: url)
        let expresion = XCTestExpectation()
        var canselables = Set<AnyCancellable>()
        ServerManager().fetchData(url: auth).sink { completion in
            print(completion)
        } receiveValue: { (value: Foursquare) in
            expresion.fulfill()
        }.store(in: &canselables)
        wait(for: [expresion], timeout: 15)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
